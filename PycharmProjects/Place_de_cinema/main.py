# Place de cinema

wallet = 20
print("You have 20 euros in your wallet.")

print("Welcome to the cinema. Which movie do you want to see, Avatar, Titanic or Batman? : ")
movieOne = ("Titanic")
movieTwo = ("Avatar")
movieThree = ("Batman")

choose = input("Choose a movie from the list:")

while choose != movieOne or movieTwo or movieThree:
    if choose == movieOne :
        print("You want to see", choose)
        break
    elif choose == movieTwo:
        print("You want to see", choose)
        break
    elif choose == movieThree:
        print("You want to see", choose)
        break
    else:
        print("Your answer is not valid. Try again")
        choose = input("Choose a movie from the list:")

# Trouver comment insérer "your answer is not valid" si la réponse demandée n'est pas un entier

ageClient = int(input("How old are you? : "))
if ageClient > 120:
    print("Your age is not valid. Please try again.")
    ageClient = int(input("How old are you? : "))
else:
    print("Your are", ageClient,"years old")

# Récolter l'âge de la personne

if ageClient < 18:
    ticketMinor = 7
    print("You are a minor, the price of your ticket is:", ticketMinor, "euros")
    wallet = wallet - ticketMinor
    print("You have paid", ticketMinor, "euros. You have {}".format(wallet), "euros left in your wallet")
else:
    ticketAdult = 12
    print("You are an adult, the price of your ticket is:", ticketAdult, "euros")
    wallet = wallet - ticketAdult
    print("You have paid", ticketAdult, "euros. You have {}".format(wallet), "euros left in your wallet")

# Demander si le client veut du popcorn

popcorn = input("Do you want popcorn[oui/non]: ")

while popcorn != "oui" or "non":
    if popcorn == "oui":
        print("You want popcorn. 5 euros please.")
        wallet = wallet - 5
        print("You have paid 5euros. You have {}".format(wallet), "euros left in your wallet. Good Movie.")
        break
    elif popcorn == "non":
        print("You don't want popcorn. You have {}".format(wallet), "euros left in your wallet. Good Movie.")
        break
    else:
        print("Your answer is not valid. Try again")
        popcorn = input("Do you want popcorn[oui/non]: ")





