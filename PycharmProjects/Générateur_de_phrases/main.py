from random import shuffle

# Générateur de phrases

# demander en console une chaîne de la forme mot1/mot2/mot3/mot4

string = input("enter a character string in the form of word1/word2/word3/word4: ")

# transformer cette chaîne en une liste

words = string.split()
print(words)
# la mélanger

shuffle(words)
print(words)

# si le nombre d'éléments de cette liste est inférieur à 10: afficher les deux premiers mots

if len(words) < 10:
    print(words[0:2])

# si le nombre d'éléments de cette liste est égal ou supérieur à 10: afficher les trois derniers mots

if len(words) >= 10:
    print(words[-3:])