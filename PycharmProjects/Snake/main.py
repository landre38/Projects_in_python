import sys, random
import pygame

class Jeu:
    # va contenir toutes les variables et les fonctions utiles pour le bon déroulement du jeu

    def __init__(self):
        self.clock = pygame.time.Clock()
        self.ecran = pygame.display.set_mode((800, 600))

        pygame.display.set_caption('Jeu Snake')
        self.jeu_encours = True

        #creer les variables de position et de direction du serpent
        self.serpent_position_x = 300
        self.serpent_position_y = 300
        self.serpent_direction_x = 0
        self.serpent_direction_y = 0
        self.serpent_corps = 10

        # creer la position de la pomme

        self.pomme_position_x = random.randrange(110,690,10)
        self.pomme_position_y = random.randrange(110,590,10)
        self.pomme = 10

        # creer une liste qui recense toutes les positions du serpent
        self.positions_serpent = []

        # creer la variable en rapport avec la taille du serpent
        self.taille_du_serpent = 1

        self.ecran_du_debut = True

        # charger l'image de la tete du serpent
        self.image_la_tete_du_serpent = pygame.image.load('la_tete_du_serpent.png')

        # Charger l'image

        self.image = pygame.image.load('snake-game.jpg')

        # retrecir l'image

        self.image_titre = pygame.transform.scale(self.image,(800,600))

        # creer la variable score

        self.score = 0




    def fonction_principale(self):
        # permer de gérer les évènements et d 'afficher certains composants grace au whileloop

        while self.ecran_du_debut:

            for evenement in pygame.event.get():
                if evenement.type == pygame.QUIT:
                    sys.exit()

                if evenement.type == pygame.KEYDOWN:
                    if evenement.key == pygame.K_RETURN:

                        self.ecran_du_debut = False

                self.ecran.fill((0,0,0))

                self.ecran.blit(self.image_titre,(0,0,100,50))

                self.creer_message('moyenne','Le but du jeu est que le serpent se developpe',(150,30,200,5),(240,240,240))
                self.creer_message('moyenne','pour cela, il a besoin de pomme, mangez-en autant que possible!!',(90,50,200,5),(240,240,240))
                self.creer_message('grande','Appuyer sur Entrer pour commencer',(120,520,200,5),(255,255,255))

                pygame.display.flip()


        while self.jeu_encours:

            self.clock.tick(15)

            for evenement in pygame.event.get():
                if evenement.type == pygame.QUIT:
                    sys.exit()

                    # creer les evenements qui permettent de bouger le serpent

                if evenement.type == pygame.KEYDOWN:

                    if evenement.key == pygame.K_RIGHT:
                        self.serpent_direction_x = 10
                        self.serpent_direction_y = 0

                    if evenement.key == pygame.K_LEFT:
                        self.serpent_direction_x = -10
                        self.serpent_direction_y = 0

                    if evenement.key == pygame.K_DOWN:
                        self.serpent_direction_y = 10
                        self.serpent_direction_x = 0

                    if evenement.key == pygame.K_UP:
                        self.serpent_direction_y = -10
                        self.serpent_direction_x = 0

            if self.serpent_position_x <= 100 or self.serpent_position_x >= 700 \
                or self.serpent_position_y <= 100 or self.serpent_position_y >= 600:

                sys.exit()

            self.serpent_mouvement()

            # creer la condition si le serpent mange la pomme

            if self.pomme_position_y == self.serpent_position_y and self.serpent_position_x == self.pomme_position_x:
                print('OK')

                self.pomme_position_x = random.randrange(110,690,10)
                self.pomme_position_y = random.randrange(110,590,10)

                # augmenter la taille du serpent

                self.taille_du_serpent += 1

                # augmenter le score
                self.score += 1

            # creer une liste qui stocke la postion de la tete du serpent
            la_tete_du_serpent = []
            la_tete_du_serpent.append(self.serpent_position_x)
            la_tete_du_serpent.append(self.serpent_position_y)

            # append dans la liste des positions du serpent

            self.positions_serpent.append(la_tete_du_serpent)

            # conditions pour résoudre le problème des positions du serpent avec la taille du serpent
            if len(self.positions_serpent) > self.taille_du_serpent:

                self.positions_serpent.pop(0)
                print(self.positions_serpent)

            self.afficher_les_elements()

            self.creer_message('grande','Snake Game',(320,10,100,50),(255,255,255))
            self.creer_message('grande','{}'.format(str(self.score)),(375,50,50,50),(255,255,255))


            # si le serpent se mord la queue alors le jeu s'arrête

            for partie_du_serpent in self.positions_serpent[:-1]:

                if la_tete_du_serpent == partie_du_serpent:

                    sys.exit()

            # creer les limites
            self.creer_limites()

            pygame.display.flip()

    def creer_limites(self):

        pygame.draw.rect(self.ecran,(255,255,255),(100,100,600,500),3)

    def serpent_mouvement(self):

    # faire bouger le serpent

        self.serpent_position_x += self.serpent_direction_x
        self.serpent_position_y += self.serpent_direction_y

    def afficher_les_elements(self):

        self.ecran.fill((0, 0, 0))

        # Afficher le serpent

        #pygame.draw.rect(self.ecran, (0, 255, 0),
                         #(self.serpent_position_x, self.serpent_position_y, self.serpent_corps, self.serpent_corps))
        self.ecran.blit(self.image_la_tete_du_serpent,(self.serpent_position_x,self.serpent_position_y,self.serpent_corps,self.serpent_corps))

        # Afficher la pomme
        pygame.draw.rect(self.ecran, (255, 0, 0),
                         (self.pomme_position_x, self.pomme_position_y, self.pomme, self.pomme))

        # Afficher les autres parties du serpent
        for partie_du_serpent in self.positions_serpent[:-1]:
            pygame.draw.rect(self.ecran, (0, 255, 0),
                             (partie_du_serpent[0], partie_du_serpent[1], self.serpent_corps, self.serpent_corps))

    # creer une foncion qui permet d'afficher les messages

    def creer_message(self,font,message,message_rectangle,couleur):

        if font == 'petite':
            font = pygame.font.SysFont('Lato',20,False)

        elif font == 'moyenne':
            font = pygame.font.SysFont('Lato',30,False)

        elif font == 'grande':
            font = pygame.font.SysFont('Lato',40,True)

        message = font.render(message,True,couleur)

        self.ecran.blit(message,message_rectangle)



if __name__ == '__main__':

    pygame.init()
    Jeu().fonction_principale()
    pygame.quit()