import pygame, sys

pygame.init()
screen = pygame.display.set_mode((800,500))
clock = pygame.time.Clock()

player = pygame.image.load('1.png')
player = pygame.transform.scale(player, (500,500))
player_rect = player.get_rect()

def draw_bg():
    screen.blit(bg_surface, (bg_x_pos, 0))
    screen.blit(bg_surface, (bg_x_pos + 800, 0))

bg_surface = pygame.image.load('BG-sky.png')
bg_surface = pygame.transform.scale(bg_surface, (800,500))
bg_x_pos = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    screen.blit(player ,player_rect)
    bg_x_pos -= 1
    draw_bg()
    if bg_x_pos <= -800:
        bg_x_pos = 0

    pygame.display.update()
    clock.tick(120)