import pygame
import sys

class Grille :

    def __init__(self,ecran):
        self.ecran = ecran
        self.lignes = [((200,0),(200,600)),
                       ((400,0),(400,600)),
                       ((0,200),(600,200)),
                       ((0,400),(600,400)),]

        # initier la grille
        self.grille = [[None for x in range(0,3)] for y in range (0,3)]

        # initier une variabe pour vérifier si le compteur est ON
        self.compteur_on = False

    def afficher(self):

        for ligne in self.lignes:

            pygame.draw.line(self.ecran,(0,0,0),ligne[0],ligne[1],2)

        # afficher les X et O

        for y in range(0,len(self.grille)):
            for x in range(0, len(self.grille)):
                if self.grille[y][x] == 'X':
                    pygame.draw.line(self.ecran,(0,0,0), (x*200, y*200), (200 +(x*200), 200 + (y*200)),3)
                    pygame.draw.line(self.ecran, (0, 0, 0), ((x * 200), 200 + (y * 200)),(200 + (x*200),(y*200)), 3)

                elif self.grille[y][x] == 'O':

                    pygame.draw.circle(self.ecran, (0,0,0), (100 + (x *200), 100+(y* 200)), 100, 3)


# print la grille
    def print_grille(self):
        print(self.grille)

# fixer les valeurs

    def fixer_la_valeur(self,x,y,valeur):

        # condition si une case possèdel valeur None
        if self.grille[y][x] == None:
            self.grille[y][x] = valeur

            # le compteur est ON
            self.compteur_on = True




class Jeu :

    def __init__(self):

        self.ecran = pygame.display.set_mode((600,600))
        pygame.display.set_caption('Tic Tac Toe')
        self.jeu_encours = True
        self.grille = Grille(self.ecran)

        # fixer les variables X et O
        self.player_X = 'X'
        self.player_O = 'O'

        # fixer le compteur
        self.compteur = 0

    def fonction_principale(self):

        while self.jeu_encours:

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    sys.exit()

                # ajouter l'evenement qui correspond au clique droit
                if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:

                    # obtenir la position de la souris
                    position = pygame.mouse.get_pos()
                    position_x,position_y = position[0]//200, position[1]//200


                    # condition si le compteur est pair ou pas
                    print(self.compteur,self.compteur%2)
                    if self.compteur %2 == 0:
                        self.grille.fixer_la_valeur(position_x,position_y,self.player_X)
                    else:
                        self.grille.fixer_la_valeur(position_x,position_y,self.player_O)

                    # condition si le compteur ON est vrai
                    if self.grille.compteur_on:
                        self.compteur += 1

                    # fixe le compteur ON = Faux
                        self.grille.compteur_on = False




            self.ecran.fill((240,240,240))
            self.grille.afficher()

            pygame.display.flip()


if __name__ == '__main__':
    pygame.init()
    Jeu().fonction_principale()
    pygame.quit()
