# écrire un programme qui demande l'année de naissance d'une personne et qui calcule et affiche l'âge de la personne

name = input("Enter your name: ")

birth = int(input("Enter your birth year: "))

age = 2022 - int(birth)

print("Hey", name,", you are", age,"years old")
