from random import randint

price = randint(1, 10)

guess = int(input("\nThe right price is between 1 and 10. \n Guess the right price: "))

attempt = 0

while True:

    if guess < price:
        print("\nThe right price is higher")

    elif guess > price:
        print("\nThe right price is lower")

    else :
        print("\nCongratulations, you win. You found the right price in {} tries!".format(attempt + 1, int(attempt)+1))
        break

    attempt += 1
    if attempt != 5:
        print("\nYou have", 5 - attempt, "tries left \n")
    if attempt == 5:
        print("\nYou used five tries, you loose")
        break

    guess = int(input("The right price is between 1 and 10. \n Guess the right price: "))