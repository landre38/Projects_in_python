import pygame, sys

def ball_animation():
    # Animation de la balle

    global ball_speed_x, ball_speed_y
    ball.x += ball_speed_x
    ball.y += ball_speed_y

    if ball.top <= 0 or ball.bottom >= screen_height:
        ball_speed_y *= -1
    if ball.left <= 0 or ball.right >= screen_width:
        ball_speed_x *= -1

    # Collisions // rect1.colliderect(rect2) // no collision: return False // collision: returns True

    if ball.colliderect(player) or ball.colliderect(opponent):
        ball_speed_x *= -1

def player_animation():

    player.y += player_speed
    if player.top <= 0:
        player.top = 0
    if player.bottom >= screen_height:
        player.bottom = screen_height


pygame.init()
clock = pygame.time.Clock() # Créer un objet pour aider à suivre le temps

# Configuration de la fenêtre principale

screen_width = 1280
screen_height = 960
screen = pygame.display.set_mode((screen_width, screen_height)) # Initialiser une fenêtre ou un écran pour l'affichage
pygame.display.set_caption # Définis la légende de la fenêtre actuelle

# Rectangle du jeu pygame.Rect(x,y,width,height)

ball = pygame.Rect(screen_width/2 - 15,screen_height/2 - 15,30,30)
player = pygame.Rect(screen_width - 20,screen_height/2 - 70,10,140)
opponent = pygame.Rect(10, screen_height/2 - 70,10,140)

bg_color = pygame.Color('grey12') # objet pygame pour les représentations de couleurs
light_grey = (200,200,200)

# Vitesse de la balle

ball_speed_x = 7
ball_speed_y = 7
player_speed = 0

# Gestion des entrées

while True:
    for event in pygame.event.get(): # obtient les événements qui attendent sur la queue. S’il n’y en a pas, retourne un ensemble vide
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN:
                player_speed +=7
            if event.key == pygame.K_UP:
                player_speed -=7
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN:
                player_speed -=7
            if event.key == pygame.K_UP:
                player_speed +=7


    ball_animation()
    player_animation()


    # Visuels
    screen.fill(bg_color)
    pygame.draw.rect(screen, light_grey, player)
    pygame.draw.rect(screen, light_grey, opponent)
    pygame.draw.ellipse(screen, light_grey, ball) # trace une ellipse
    pygame.draw.aaline(screen, light_grey, (screen_width/2,0), (screen_width/2,screen_height)) # trace une ligne droite

# Mise à jour de la fenêtre

    pygame.display.flip() # Mise à jour de la surface d'affichage complète à l'écran
    clock.tick(60)
