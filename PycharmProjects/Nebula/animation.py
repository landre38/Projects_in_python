import pygame

# definir une classe qui va s'occuper des animations
class AnimateSprite(pygame.sprite.Sprite):

    # definir les choses à faire à la création de l'entité
    def __init__(self, sprite_name):
        super().__init__()
        self.image = pygame.image.load(f'Wraith/{sprite_name}.png')
        self.current_image = 0
        self.images = animations.get('Wraith')

    # definir une methode pour animer le sprite
    def animate(self):
        # passer à l'image suivante
        self.current_image += 1

        # verifier si on atteint la fin de l'animation
        if self.current_image >= len(self.images):
            # remettre l'animation au départ
            self.current_image = 0

        # modifier l'image précédente par la suivante
        self.images[self.current_image]


# definir une fonction pour charger les images d'un sprite
def load_animation_images(sprite_name):
    # charger les 24 images de ce sprite dans le dossier correspondant
    images = []
    # recuperer le chemin du dossier pour ce sprite
    path = f"Wraith/{sprite_name}"

    # boucler sur chaque image dans ce dossier
    for num in range(1,12):
        image_path = path + str(num) + '.png'
        images.append(pygame.image.load(image_path))

        # renvoyer le contenu de la liste d'images
        return images


# definir un dictionnaire qui va contenir les images chargées de chaque sprite
animations = {
    'Wraith': load_animation_images("Wraith")
}
