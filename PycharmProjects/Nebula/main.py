import pygame
from game import Game
pygame.init()


# generer la fenetre du jeu
pygame.display.set_caption("Nebula")
screen = pygame.display.set_mode((880,520))

# charger l'arrière plan du jeu
background = pygame.image.load('Cartoon_Forest_BG_02.png')
background = pygame.transform.scale(background, (880,520))
rect = background.get_rect()
rect = rect.move((0,0))

# charger le jeu
game = Game()

running = True

# boucle tant que la condition est vrai
while running:

    # appliquer l'arrière plan
    screen.blit(background, rect)

    # appliquer l'image du joueur
    assert isinstance(screen, object)
    screen.blit(game.player.image, game.player.rect)

    # actualiser la barre de vie du joueur
    game.player.update_health_bar(screen)

    # récupérer les projectiles du joueur
    for projectile in game.player.all_projectiles:
        projectile.move()

    # récupérer les monstres de notre jeu
    for monster in game.all_monsters:
        monster.forward()
        monster.update_health_bar(screen)

    # appliquer l'ensemble des images du groupe projectile
    game.player.all_projectiles.draw(screen)

    # appliquer l'ensemble des images du groupe monstre
    game.all_monsters.draw(screen)

    # verifier si le joueur souhaite aller à gauche ou à droite, en haut ou en bas
    if game.pressed.get(pygame.K_RIGHT) and game.player.rect.x < 820 :
        game.player.move_right()
    elif game.pressed.get(pygame.K_LEFT) and game.player.rect.x > 0:
        game.player.move_left()
    elif game.pressed.get(pygame.K_UP) and game.player.rect.y > -15:
        game.player.move_up()
    elif game.pressed.get(pygame.K_DOWN) and game.player.rect.y < 475:
        game.player.move_down()



    # mettre à jour l'écran
    pygame.display.flip()

    # si le joueur ferme cette fenetre
    for event in pygame.event.get():
        # que l'évènement correspond à la fermeture de fenêtre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
        # detecter si joueur lache une touche du clavier
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True

            # detecter si la touche espace est enclenchée
            if event.key == pygame.K_SPACE:
                game.player.launch_projectile()

        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False

