import pygame
from projectile import Projectile
import animation

# creer une premiere classe qui va representer notre joueur
class Player(animation.AnimateSprite):

    def __init__(self, game):
        super().__init__("Wraith1")
        self.game = game
        self.health = 100
        self.max_health = 100
        self.attack = 10
        self.velocity = 5
        self.all_projectiles = pygame.sprite.Group()
        self.image = pygame.transform.scale(self.image, (200,150))
        self.rect = self.image.get_rect()
        self.rect.x = 50
        self.rect.y = 350

    def damage(self, amount):
        if self.health - amount > amount:
            # infliger les dégâts
            self.health -= amount

    def update_animation(self):
        self.animate()

    def update_health_bar(self, surface):
        # definir une couleur pour notre jauge de vie (vert clair)
        bar_color = (111,210,46)
        # definir une couleur pour l'arrière plan de la jauge (gris foncé)
        back_bar_color = (60,63,60)

        # definir la position de notre jauge de vie ainsi que sa largeur et son épaisseur
        bar_position = [self.rect.x + 55, self.rect.y - 5, self.health, 7]
        # definir la position de l'arriere plan de notre jauge de vie
        back_bar_position = [self.rect.x + 55, self.rect.y - 5, self.max_health, 7]

        # dessiner notre barre de vie
        pygame.draw.rect(surface, back_bar_color, back_bar_position)
        pygame.draw.rect(surface, bar_color, bar_position)

    def launch_projectile(self):
        # creer une nouvelle instance de la classe projectile
        self.all_projectiles.add(Projectile(self))

    def move_right(self):
        # si le joueur n'est pas en collision avec le monstre
        if not self.game.check_collision(self, self.game.all_monsters):
            self.rect.x += self.velocity

    def move_left(self):
        self.rect.x -= self.velocity

    def move_up(self):
        self.rect.y -= self.velocity

    def move_down(self):
        self.rect.y += self.velocity