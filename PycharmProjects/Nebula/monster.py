import pygame
import random


# créer une classe qui va gérer la notion de monstre sur notre jeu
class Monster(pygame.sprite.Sprite):

    def __init__(self, game):
        super().__init__()
        self.game = game
        self.health = 100
        self.max_health = 100
        self.attack = 0.3
        self.image = pygame.image.load('Wraith_03_Idle_000.png')
        self.image = pygame.transform.scale(self.image, (200,150))
        self.rect = self.image.get_rect()
        self.rect.x = 750 + random.randint(0,300)
        self.rect.y = 350
        self.velocity = random.randint(1, 3)

    def damage(self, amount):
        self.health -= amount

        # verifier si son nouveau nombre de points de vie est inférieur ou égal à 0
        if self.health <=0:
            # réapparaître comme un nouveau monstre
            self.rect.x = 1000 + random.randint(0, 300)
            self.velocity = random.randint(1, 3)
            self.health = self.max_health

    def update_health_bar(self, surface):
        # definir une couleur pour notre jauge de vie (vert clair)
        bar_color = (111,210,46)
        # definir une couleur pour l'arrière plan de la jauge (gris foncé)
        back_bar_color = (60,63,60)

        # definir la position de notre jauge de vie ainsi que sa largeur et son épaisseur
        bar_position = [self.rect.x + 55, self.rect.y - 5, self.health, 5]
        # definir la position de l'arriere plan de notre jauge de vie
        back_bar_position = [self.rect.x + 55, self.rect.y - 5, self.max_health, 5]

        # dessiner notre barre de vie
        pygame.draw.rect(surface, back_bar_color, back_bar_position)
        pygame.draw.rect(surface, bar_color, bar_position)

    def forward(self):
        # le déplacement ne se fait que s'il n'y a pas de collision avec un groupe de joueur
        if not self.game.check_collision(self,self.game.all_players):
            self.rect.x -= self.velocity
        # si le monstre est en collision avec le joueur
        else:
            # Infliger des dégâts (au joueur)
            self.game.player.damage(self.attack)