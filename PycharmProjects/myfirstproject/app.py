
from tkinter import *
import webbrowser

def open_channel_youtube():
    webbrowser.open_new("youtube.fr")

window = Tk()

window.title("My application")
window.geometry("720x480")
window.minsize(480, 360)
window.iconbitmap("logo.ico")
window.config(background='black')

frame = Frame(window, bg= 'black')

label_title = Label(frame, text="Bienvenue sur l'application", font=("Courrier", 40), bg ='black', fg= 'white')
label_title.pack()

label_subtitle = Label(frame, text="Hey, salut à tous!", font=("Courrier", 25), bg ='black', fg= 'white')
label_subtitle.pack()

yt_button= Button(frame, text="Ouvrir youtube", font=("Courrier", 25), bg ='white', fg= 'black', command=open_channel_youtube)
yt_button.pack(pady=25, fill=X )

frame.pack(expand= YES)

window.mainloop()
