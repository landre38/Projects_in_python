import random
import string # recupérer des caractères de la table ascii
import re

# creer une fonction ui va se charger de générer cette chaîne (code secret)
def generer_code_debloquage():
    # format du code de déblocage:  AAA-2222-XX
    lettres = string.ascii_uppercase

    mot = ''.join(random.choice(lettres) for i in range(4))
    numeros = random.randint(1000, 9999)
    chars =''.join(random.choice(lettres) for i in range(2))

    # choisir une lettre
    code = mot + "-" + str(numeros) + '-' + chars
    print("Votre code débloquage est: ", code)
    return code

code = generer_code_debloquage()


# creer une fonction afficher_parking pour afficher les statuts de chaque emplacement
def afficher_parking():
    for num_etage, etage in enumerate(parking, start=1):
        for num_place, place in enumerate(etage, start =1):
            resultat = ("Non Disponible","Disponible")[place == 'D']
            resultat = (resultat, "Réservée")[place == 'H']
            print("Etage n° -", num_etage,"place n°", num_place, resultat)


# afficher un message de bienvenue
print("Bienvenue au niveau - 1, que souhaitez-vous faire?")

# creation de la liste
emplacements = 27
etages = 3
parking = [['D']* emplacements] * etages

# creer une liste qui contient un dictionnaire par étage avec les  codes à débloquer
code_debloquage = [
    {},
    {},
    {}
]

# pour chaue étage
for num_etage, etage in enumerate(parking):
    # générer 2 emplacements prioritaires sur chaque étage
    emplacement1_hasard = random.randint(1, len(parking[num_etage]) - 1)
    emplacement2_hasard = random.randint(1, len(parking[num_etage]) - 1)
    parking[num_etage][emplacement1_hasard] = 'H'
    parking[num_etage][emplacement2_hasard] = 'H'

afficher_parking()

# récupérer l'emplacement n 3 de mon parking
emplacement_selection = parking[2]

# garer une voiture au 1er emplacement (n 0)
parking[0][0] = 'V'

# creer une boucle qui ne va jamais s'arrêter
while True:

    print("Choisissez un numéro d'étage")

    # appeler la fonction pour afficher le parking
    # afficher_parking()

    # proposer à notre client de choisir un étage
    choix_etage = int(input()) - 1

    if choix_etage >= 0 and choix_etage < len(parking):
        print("Vous avez choisi l'étage numéro", choix_etage + 1)

        # proposer à notre client de faire une action
        print("1: Garer une voiture, 2: Récupérer une voiture")
        choix = int(input())

        # verifier le choix
        if choix == 1:
            print("Vous avez choisi de garer une voiture, à quel emplacement souhaitez-vous la mettre?")
            choix_emplacement = int(input()) - 1

            # vérifier si la place est disponible
            if len(parking[choix_etage]) > choix_emplacement >= 0:

                # verifier si l'emplacement est H
                if parking[choix_etage][choix_emplacement] == 'H':
                    print("Quel est votre code prioritaire")
                    # demander son code sous le format suivant HP-1111-X3
                    choix_code_h = input()

                    # verification du code
                    regex = "^HP-[1-9]*-[a-z-A-Z]*"
                    verif = re.search(regex, choix_code_h)

                    if verif is None:
                        print("Vous n'avez pas le bon formatage")
                    else:
                        print("c'est ok!")
                        print("Vous avez pris la place numéro", choix_emplacement + 1)
                        parking[choix_etage][choix_emplacement] = 'V'
                        # générer le code secret
                        code_secret = generer_code_debloquage()
                        code_debloquage[choix_etage][choix_emplacement] = code_secret

                elif parking[choix_etage][choix_emplacement] == 'D':
                    print("Vous avez pris la place numéro", choix_emplacement + 1)
                    parking[choix_etage][choix_emplacement] = 'V'
                    # générer le code secret
                    code_secret = generer_code_debloquage()
                    code_debloquage[choix_etage][choix_emplacement] = code_secret

                else:
                    print("Emplacement non disponible")


        elif choix == 2:
            print("Récupérer une voiture, mettre le numéro de la place:")

            choix_emplacement = int(input()) - 1

            # vérifier si la place existe
            if len(parking[choix_etage]) > choix_emplacement >= 0:
                if parking[choix_etage][choix_emplacement] == 'V':

                    print("Entrer le code secret")

                    # demander le code secret de la voiture
                    choix_code_secret = input()

                    # verifier si le code secret est bon
                    code_secret_a_trouver = code_debloquage[choix_etage][choix_emplacement]

                    # comparer
                    if choix_code_secret == code_secret_a_trouver:
                        print("Véhicule récupéré")
                        parking[choix_etage][choix_emplacement] == 'D'
                        print("L'emplacement numéro", choix_emplacement + 1, "est désormais disponible")
                    else:
                        print("Code incorrecte! Stop!")

        else:
            print("Erreur, vous devez écrire 1 ou 2")

        # afficher le statut du parking
        # afficher_parking()

    else:
        print("Etage non existant!")
