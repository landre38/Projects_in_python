import pygame
import glob

SIZE = (600,600)

class MySprite(pygame.sprite.Sprite):
    def __init__(self, action):
        super(MySprite, self).__init__()
        lis_img = glob.glob(f"png\\{action}*.png")
        len_img1 = len(list_img[0])
        self.images = [pygame.image.load(img) for img in list_img if len_img1 == len(img)]
        self.images.extend([pygame.image.load(img) for img in list_img if len(img) > len_img1])
        self.index = 0
        self.rect = pygame.Rect(5,5, 150, 150, 198)

    def update(self):
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]


def action(movement):
    sprite = MySprite(movement)
    group = pygame.sprite.Group(sprite)

def main():
    pygame.init()
    screen = pygame.display.get_mode(SIZE)
    pygame.display.set_caption("Spectrum")
    sprite = MySprite("idle")
    group = pygame.sprite.Group(sprite)
    clock = pygame.time.Clock()

    loop = 1
    while loop