import random
import time
import tkinter as tk

color = "#08DD6B"


# on crée la fenêtre
fenetre = tk.Tk()
fenetre.geometry("600x300")
fenetre.title("Le Juste Prix")
fenetre.resizable(width=False, height=False)
fenetre.config(bg=color)

# ajouter une entrée pour écrire
entree_proposition = tk.Entry(fenetre)
entree_proposition.pack()

# ajouter le bouton
bouton = tk.Button(fenetre, text="Valider")
bouton.pack()

# ajouter un intitulé pour afficher des informations
info = tk.Label(fenetre, text="Bonne chance...", bg=color)
info.pack()

# ajouter nombre de tentatives
tentatives_texte =tk.Label(fenetre,text="5 tentatives", bg=color)
tentatives_texte.place(x=500,y=20)

# ajouter temps restant
tentatives_texte = tk.Label(fenetre,text="60s restantes", bg= color)
tentatives_texte.place(x=20,y=20)


#afficher
fenetre.mainloop()

# afficher le message de bienvenue
print("Bienvenue au jeu du juste prix, tu dois deviner le prix auquel je pense. \nLe prix est entre 1 et 100")

# choisir un nombre entre 1 et 100
nombre_hasard = random.randint(1,100)

nombre_proposition = 0

# compteur tentatives
tentatives = 25

# recupere le temps actuel
temps = time.time()
print(temps)

#boucler tant que
while nombre_proposition != nombre_hasard and tentatives > 0:

    # afficher le nombre au hasard
    print("Nombre à trouver: {}".format(nombre_hasard))

    # proposer à mon joueur d'entrer une proposition
    print("Entrez votre proposition:")

    proposition = input()

    # verification
    if proposition.isdigit():
        print("C'est un nombre")

        # transformation de proposition en entier
        # cast
        nombre_proposition = int(proposition)

        # verifier si le temps n'est pas dépassé
        temps_passe = time.time() - temps

        #  la personne a dépassé 1 mn
        if temps_passe >= 60:
            print("Temps écoulé")
            break

        # verifier si le nombre proposition est plus petit que le nombre à trouver
        if nombre_proposition < nombre_hasard:
            print("C'est plus")
        elif nombre_proposition > nombre_hasard:
            print("C'est moins")

        tentatives -= 1 # enlever 1 tentative
        print("Tentatives : ",tentatives)

    else:
        print("Tu dois entrer un nombre")

if tentatives == 0:
    print("C'est perdu ! Trop de tentatives, le nombre était", nombre_hasard)
else:
    print("C'est gagné")

