import math
import time

# creer une classe qui va gérer la notion de lave linge
class LaveLinge:

    # creer le constructeur
    def __init__(self, tours_minutes, contenance_kg):
        self.tours_minutes = 1400
        self.contenance_kg = 8
        self.contenance_actuel_kg = 0
        self.temperature_actuel = 60
        self.duree_machine_default = 35
        self.duree_machine = 35
        print("Nouvelle machine ajoutée à la laverie")
        print("tours min: ", self.tours_minutes, "/ contenance:", contenance_kg,"kg")

    # methodes
    def inserer_linge(self, poids_kg):
        print("Vous ouvrez la machine et vous entrez un total de", poids_kg, "kg")

        # Verification
        if poids_kg <= self.contenance_kg:
            print("Ok! Le linge est à l'intérieur")
            self.contenance_actuel_kg = poids_kg
        else:
            print("Ah non! La machine est trop petite")



    # creer une fonction pour démarrer la machine
    def demarrage(self, poids_kg):

        # verifier si la machine est vide
        if self.contenance_actuel_kg != 0:

            print("On essaie de démarrer la machine avec", poids_kg, "KG")

            # creer une variable pour stocker le temps par défaut de la machine
            compteur_tours = 0

            # une boucle tant que la machine n'est pas arrivé à zéro
            while self.duree_machine > 0:
                print(str(self.duree_machine) + "s")
                self.duree_machine -= 1
                compteur_tours += 1400
                time.sleep(1)  # attendre une seconde avant de recommencer le tour

            # afficher nombre de tours minutes
            print("Fin", compteur_tours, "tours minutes")
        else:
            print("Démarrage impossible! Vous n'avez pas mis de linge")

    def stop(self):

        if self.duree_machine > 0:
            print("Arrêt ok!")
            self.duree_machine = 0
        else:
            print("Arret impossible, lave linge en cours d'utilisation")




# afficher un message dans la console
print("Ouverture de la machine à laver")

# appeler la fonction pour démarrer la machine à laver
machine = LaveLinge(1200, 12)

# demander le poids en kg
kg = input("Quel est le poids total en kg de votre linge à laver\n")

# dictionnaire avec nos vêtements et leurs poids
vetements = {
    "chemise rouge bleuté": 1,
    "manteau": 6,
    "chaussettes": 4,
    "jean de yannis": 18
}
total_kg = 0.0
for nom_du_vetement in vetements:
    total_kg += vetements[nom_du_vetement]

print("Vous avez", total_kg,"kg de vetements")



print(kg)

# verifier que la valeur entrée est un nombre
kg_f = float(kg)
print("Vous avez", total_kg, "kg de vêtements")

# calculer combien il faut de machines
machines = math.ceil(total_kg/ 8)
print("Vous devez faire", machines, "machines")

# calculer la consommation d'eau
consommation_eau = machines * 60
print("La consommation d'eau pour", machines, "machines est de", consommation_eau, "L")

# appeler la méthode insérer linge
machine.inserer_linge(total_kg)

# appeler la méthode démarrer la machine
machine.demarrage(poids_kg=8)

time.sleep(3)

machine.stop()

