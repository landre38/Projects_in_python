print("Bienvenue au cinema, voici les films à l'affiche")

films = ["Voyage au centre du html","Les 9 jsons cachés","Algobox"]
print(films)
print("le film Voyage au centre du html possède le numéro:", (films.index("Voyage au centre du html") + 1))
print("le film Les 9 jsons cachés possède le numéro:", (films.index("Les 9 jsons cachés") + 1))
print("le film Algobox possède le numéro:", (films.index("Algobox") + 1))

premier_film = {"titre": "Voyage au centre du html", "horaire":"16h", "places":"100" }
second_film = {"titre": "Les 9 jsons cachés", "horaire":"18h", "places":"150" }
troisieme_film = {"titre": "Algobox", "horaire":"20h", "places":"200" }

print("Le film n°",films.index("Voyage au centre du html")+1,"titre:","Voyage au centre du html,", "seance de", premier_film["horaire"],
      "(",premier_film["places"], "places disponibles",")")
print("Le film n°",films.index("Les 9 jsons cachés")+1,"titre:","Les 9 jsons cachés,", ", seance de", second_film["horaire"],
      "(",second_film["places"], "places disponibles",")")
print("Le film n°",films.index("Algobox") +1, "titre:","Algobox,", "seance de", troisieme_film["horaire"],
      "(",troisieme_film["places"], "places disponibles",")")

choix_du_film = input("Choisissez le numéro du film que vous voulez voir:")

