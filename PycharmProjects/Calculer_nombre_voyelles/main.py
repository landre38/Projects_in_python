# fonction pour calculer le nombre de voyelles dans un mot

word = "word"

while word != "end":

    word = input("Enter a word : ")

    n = len(word)

    vowels = "aeiouyAEIOUY"

    consonants = "zrtpqsdfghjklmwxcvbnZRTPQSDFGHJKLMWXCVBN"

    numberVowels = 0

    numberConsonants = 0

    for i in range(0,n):
        if(word[i] in vowels):
            numberVowels = numberVowels + 1
    if vowels == 0:
        print("this word has no vowel")
    else:
        print("this word has", numberVowels,"vowels.")
    for i in range(0,n):
        if(word[i] in consonants):
            numberConsonants = numberConsonants + 1
    if consonants == 0:
        print("this word has no consonants")
    else:
        print("this word has", numberConsonants, "consonants.")




