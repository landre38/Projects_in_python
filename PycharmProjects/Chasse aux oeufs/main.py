from panier import Panier
from oeuf import OeufChocolat # on importe la classe oeuf
import pygame
pygame.init() # charger les composants

# definir les dimensions
largeur = 800
hauteur = 480

# creer la fenetre

fenetre = pygame.display.set_mode((largeur, hauteur)) # on definit la taille
pygame.display.set_caption('Chasse aux Oeufs')


# maintenir la fenetre du jeu en éveil pour ne pas qu'elle se ferme
running = True

# charger l'image de l'arrière plan
fond = pygame.image.load('fond.jpg')
sol = pygame.image.load('sol.png')
barre_chocolat = pygame.image.load('chocolate.png')

# redimensionner la barre en chocolat
barre_chocolat = pygame.transform.scale(barre_chocolat, (60,60))

# creer un dictionnaire qui va contenir en temps réel les touches enclenchées par le joueur
touches_actives = {
    112: True
}

# creer le panier du joueur
panier = Panier(largeur, hauteur)

# creer la couleur de la barre de chocolat
chocolat_couleur = (87,64,53)

# creer un groupe qui va contenir plusieurs oeufs en chocolat
oeufs = pygame.sprite.Group()
oeufs.add(OeufChocolat(largeur, hauteur, panier))
oeufs.add(OeufChocolat(largeur, hauteur, panier))

# tant que la fenêtre est active, on boucle des instructions à chaque fois
while running:

    # actualiser toutes les images qui sont sur le jeu
    fenetre.blit(fond,(0,0))
    oeufs.draw(fenetre)
    fenetre.blit(panier.image, panier.rect)
    fenetre.blit(sol, (0,0))

    largeur_chocolat = panier.points * 3 - 20

    # dessiner l'arriere de la jauge
    pygame.draw.rect(fenetre, (37,37,37), [10, hauteur - 50, largeur - 20, 32])

    # dessiner le rectangle de chocolat
    pygame.draw.rect(fenetre, chocolat_couleur, [10, hauteur - 50, largeur_chocolat, 32])

    # on place la barre de chocolat
    fenetre.blit(barre_chocolat, (largeur_chocolat - barre_chocolat.get_width() / 2, 420))

    # recupérer tous les oeufs depuis tout le groupe de sprite
    for oeuf in oeufs:
        oeuf.gravity()

    # detecter quelle est la touche activée par le joueur
    if touches_actives.get(pygame.K_RIGHT): # si la touche de droite est active
        panier.deplacement_droite()
    elif touches_actives.get(pygame.K_LEFT): # si la touche de gauche est active
        panier.deplacement_gauche()

    # mettre à jour l'écran du jeu
    pygame.display.flip()

    # boucler sur les évènements actifs du joueur
    for evenement in pygame.event.get():
        # si l'evenement c'est la fermeture de fenêtre
        if evenement.type == pygame.QUIT:
            running = False # on arrête la boucle pour que la fenetre se ferme
            quit()# on quitte le jeu
        # si l'evenement est une interaction au clavier
        elif evenement.type == pygame.KEYDOWN:
            touches_actives[evenement.key] = True # la touche est active
        elif evenement.type == pygame.KEYUP:
            touches_actives[evenement.key]= False # la touche est désactivée




