import pygame

# creer un classe qui va représenter le concept de panier

class Panier(pygame.sprite.Sprite):

    # le constructeur
    def __init__(self, largeur_ecran, hauteur_ecran):
        super().__init__()
        self.largeur_ecran = largeur_ecran
        self.points = 50 # nombre de points qu'aura le joueur
        self.maximum_points = 100
        self.image = pygame.image.load('panier.png')
        self.image = pygame.transform.scale(self.image, (120,120)) # redimensionner l'image
        self.rect = self.image.get_rect() # on lui définit un rectangle
        self.rect.x = (largeur_ecran / 2) - self.image.get_width() /2
        self.rect.y = hauteur_ecran - 195
        self.vitesse = 6 # vitesse de deplacement du panier

    # methode pour ajouter 5 points quand on attrape un oeuf
    def ajouter_points(self):
        if self.points + 5 <= self.maximum_points:
            self.points += 5

    # enlever un point
    def enlever_points(self):
        if self.points - 2 > 0:
            self.points -= 2
        else:
            # perdu
            print('Perdu')

    # methodes pour le deplacement droite
    def deplacement_droite(self):
        if self.rect.x + self.image.get_width() < self.largeur_ecran:
            self.rect.x += self.vitesse

    # methode pour le deplacement gauche
    def deplacement_gauche(self):
        if self.rect.x > 0:
            self.rect.x -= self.vitesse