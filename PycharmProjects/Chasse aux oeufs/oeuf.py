import random
import pygame # recuperer les composants


# creer une classe qui va representer l'oeuf en chocolat
class OeufChocolat(pygame.sprite.Sprite):

    # definir la fonction init qui charger les caracteristiques de notre oeuf
    def __init__(self, largeur_ecran, hauteur_ecran, panier ):
        super().__init__()
        self.vitesse_chute = random.randint(2, 3)
        self.panier = panier
        self.panier_group = pygame.sprite.Group()
        self.panier_group.add(self.panier)
        self.largeur_ecran = largeur_ecran
        self.hauteur_ecran = hauteur_ecran
        self.image = pygame.image.load('oeuf.png')
        self.image = pygame.transform.scale(self.image,(40,40))
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(20, largeur_ecran - 20)

    # teleporter respawn
    def repositionner(self):
        # retéléporter l'oeuf en haut
        self.rect.x = random.randint(0, self.largeur_ecran -40)
        self.rect.y = - self.image.get_height()
        self.vitesse_chute = random.randint(2, 3)

    # une méthode pour faire chuter l'oeuf
    def gravity(self):
        self.rect.y += self.vitesse_chute

        # s'il touche le panier
        if pygame.sprite.spritecollide(self, self.panier_group, False, pygame.sprite.collide_mask) and self.rect.y >= 360:
            print("Collision")
            self.repositionner()
            # ajouter points
            self.panier.ajouter_points()


        # s'il sort de l'écran
        if self.rect.y >= self.hauteur_ecran:
            self.repositionner()
            # enlever points
            self.panier.enlever_points()